# Adopt-me
A react application to adopt various types pets near San Francisco, CA.
<br><br>API provided by `@frontend-masters/pet` npm dependency and project learned through Frontend-masters.
<hr>

## Live App : 

Hosted at  : https://adopt-pet-01.netlify.app/

<pre>Note : Enable Insecure http request for the above site in your browser settings otherwise api request will not be served</pre>

## Development
<br>

#### 1. Clone the Repository

```Bash
git clone https://gitlab.com/rogers9798/adopt-me.git
cd adopt-me
```

#### 2. Install the dependencies

```BASH
npm install
```

#### 3. Run server:

```BASH
npm run dev
```

#### 4. Build server for production:

```BASH
parcel build src/index.html
```
<pre>Note : Parcel package needed globall for production build</pre>

# Project in Dev server :


![Front Page](./assets/s1.png)
![Animal-type:bird](./assets/s2.png)
![Details page:bird](./assets/s3.png)
![Details page:dog](./assets/s4.png)
