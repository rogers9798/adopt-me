import React from "react";
import { render } from "react-dom";
// import Pet from "./Pet";
import { Router, Link } from "@reach/router";
import SearchParams from "./searchParams";
import Details from "./Details";

const App = () => {
  // return React.createElement("div", {}, [
  //   React.createElement("h1", {}, "Adope Me!"),
  //   React.createElement(Pet, { name: "Bruno", animal: "Dog", breed: "Husky" }),
  //   React.createElement(Pet, {
  //     name: "Pepper",
  //     animal: "Bird",
  //     breed: "Cockatiel"
  //   }),
  //   React.createElement(Pet, {
  //     name: "Lucy",
  //     animal: "Squirrel",
  //     breed: "Indian"
  //   })
  // ]);

  return (
    <div>
      <header>
        <Link to="/">Adopt Me!</Link>
      </header>
      <Router>
        <SearchParams path="/" />
        <Details path="/details/:id" />
      </Router>
      {/* <Pet name="Luna" animal="Dog" breed="husky" />
      <Pet name="Bruno" animal="bird" breed="cockatiel" />
      <Pet name="Lucy" animal="Squirrel" breed="Indian" /> */}
    </div>
  );
};

render(<App />, document.getElementById("root"));
